Authors: Mattia Basaglia

Links
=====


Video Tutorials
---------------

* [Glaxnimate - Eses infor Learning](https://www.youtube.com/playlist?list=PLqlTgdmIZxTvJ98FtjTUbMVkGgA4bRAa0) (English)
* [Glaxnimate - punto edu](https://www.youtube.com/playlist?list=PLNdcfEqn_DGDxp6Vx1RRFZpfnHreXBn8R) (Spanish)

Listings
--------

* [AlternativeTo](https://alternativeto.net/software/glaxnimate/)
* [FSF Directory](https://directory.fsf.org/wiki/Glaxnimate)
* [fresh(code)](https://freshcode.club/projects/glaxnimate)
* [Open Hub](https://www.openhub.net/p/glaxnimate)
* [SourceForge](https://sourceforge.net/projects/glaxnimate/)


Reviews and Articles
--------------------

* [0.4.3 Video comparing various animation programs](https://www.youtube.com/watch?v=26V3T9QqYWw) (Russian)
* [0.3.1 Dev.to](https://dev.to/mbasaglia/glaxnimate-create-2d-vector-animations-for-the-web-2ein)
* [0.3.0 Softpedia](https://www.softpedia.com/get/Multimedia/Graphic/Graphic-Others/Glaxnimate.shtml)
* [0.3.0 Apps for my PC](http://www.appsformypc.com/2020/11/glaxnimate/)
* [0.2.0 Emojized](https://emojized.com/blog/2020/10/29/new-awesome-tool-for-animated-telegram-stickers/)
